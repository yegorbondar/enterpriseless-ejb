package smart.club.enterpriseless.dao;

import smart.club.enterpriseless.model.BaseEntity;

import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.util.List;


public abstract class BaseDao<T extends BaseEntity> {
    @PersistenceContext(unitName = "enterpriseless-world")
    private EntityManager em;

    protected Class<T> entityClass;

    public BaseDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class) genericSuperclass.getActualTypeArguments()[0];
    }

    @TransactionAttribute
    public Long save(T entity) {
        em.persist(entity);
        return entity.getId();
    }

    public T find(Long id) {
        return em.find(entityClass, id);
    }

    @TransactionAttribute
    public void remove(T entity) {
        em.remove(entity);
    }

    @TransactionAttribute
    public void save(List<T> entities){
        int counter = 0;
        for(T entity: entities) {
            counter++;

            em.persist(entity);

            if(counter == 10) {
                em.flush();
                em.clear();
            }
        }
    }

    protected EntityManager getEntityManager() {
        return em;
    }
}

package smart.club.enterpriseless.dao;

import smart.club.enterpriseless.model.Currency;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;


@Stateless
@LocalBean
public class CurrencyDao extends BaseDao<Currency> {
    public List<Currency> getLatest() {
        TypedQuery<Currency> namedQuery = getEntityManager().createNamedQuery("Currency.getLatest", Currency.class);
        getEntityManager().createNamedQuery("Currency.getLatest");
        return namedQuery.getResultList();
    }

}

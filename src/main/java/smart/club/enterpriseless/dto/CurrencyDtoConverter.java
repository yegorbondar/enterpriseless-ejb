package smart.club.enterpriseless.dto;

import smart.club.enterpriseless.model.Currency;

public class CurrencyDtoConverter {
    public static CurrencyDto toDto(Currency currency) {
        CurrencyDto result = new CurrencyDto();
        result.setCreated(currency.getCreated());
        result.setCurrency(currency.getCurrency());
        result.setRate(currency.getRate());
        return result;
    }
}

package smart.club.enterpriseless.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement(name = "currencies")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyWrapper {
    public CurrencyWrapper() {
    }

    public CurrencyWrapper(List<CurrencyDto> currencies) {
        this.currencies = currencies;
    }

    @XmlElement
    private List<CurrencyDto> currencies;

    public List<CurrencyDto> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<CurrencyDto> currencies) {
        this.currencies = currencies;
    }
}
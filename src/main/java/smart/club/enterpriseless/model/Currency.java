package smart.club.enterpriseless.model;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
        @NamedQuery(name = "Currency.getLatest",
                query = "select c from Currency c where c.id in (select max(c1.id) from Currency c1 group by c1.currency)")
)
public class Currency extends BaseEntity {

    private String currency;

    private Double rate;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return currency + " " + rate;
    }
}

package smart.club.enterpriseless.http;

import org.apache.cxf.jaxrs.client.WebClient;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class EnterpriselessHttpClient implements HttpClient {

    public String get(String url) {
        return WebClient.create(url).get(String.class);
    }
}
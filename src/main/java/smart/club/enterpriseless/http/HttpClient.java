package smart.club.enterpriseless.http;

import javax.ejb.Local;


@Local
public interface HttpClient {
    public String get(String url);
}

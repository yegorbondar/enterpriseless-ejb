package smart.club.enterpriseless.service;

import smart.club.enterpriseless.dao.CurrencyDao;
import smart.club.enterpriseless.model.Currency;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Stateless
public class ForeignExchangeService {
    @EJB
    private CurrencyDao currencyDao;

    public List<Currency> getLatest() {
        return currencyDao.getLatest();
    }

    public void storeLatest(String latestRates) {
        List<Currency> currencies = new ArrayList<Currency>();
        Matcher ratesMatcher = currencyRatePattern.matcher(latestRates);
        while (ratesMatcher.find()) {
            String currency = ratesMatcher.group(1);
            String rate = ratesMatcher.group(2);
            currencies.add(createCurrency(currency, new Double(rate)));
        }
        currencyDao.save(currencies);
    }

    private Currency createCurrency(String currencyStr, double rate) {
        Currency currency = new Currency();
        currency.setCurrency(currencyStr);
        currency.setRate(rate);
        return currency;
    }

    private Pattern currencyRatePattern = Pattern.compile("\"(\\S{3}?)\":(\\d+\\.?\\d*)");
}

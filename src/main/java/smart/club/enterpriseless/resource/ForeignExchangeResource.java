package smart.club.enterpriseless.resource;

import smart.club.enterpriseless.dto.CurrencyDto;
import smart.club.enterpriseless.dto.CurrencyDtoConverter;
import smart.club.enterpriseless.dto.CurrencyWrapper;
import smart.club.enterpriseless.http.EnterpriselessHttpClient;
import smart.club.enterpriseless.model.Currency;
import smart.club.enterpriseless.service.ForeignExchangeService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.List;

@Path(value = "/")
@Produces(value = "application/json")
public class ForeignExchangeResource {

    @EJB
    private ForeignExchangeService foreignExchangeService;

    @EJB
    private EnterpriselessHttpClient httpClient;

    @GET
    public String index() {
        return "hello";
    }

    @GET
    @Path("/latest")
    public CurrencyWrapper getLatestRates() {
        List<Currency> latestCurrencies = foreignExchangeService.getLatest();
        List<CurrencyDto> currencyDtos = new ArrayList<CurrencyDto>();
        for(Currency currency: latestCurrencies) {
            currencyDtos.add(CurrencyDtoConverter.toDto(currency));
        }

        return new CurrencyWrapper(currencyDtos);
    }

    //@POST
    @GET
    @Path("/post")
    public String postLatestRates() {
        foreignExchangeService.storeLatest(httpClient.get("http://api.fixer.io/latest"));
        return "OK";
    }
}
/*
* googles WARNUNG: "null OEJP/4.7" FAIL "Security error - [Ljava.net.URI; is not whitelisted as deserialisable, prevented before loading it." - Debug for StackTrace

http://stackoverflow.com/questions/36542297/security-error-starting-tomee-plume-1-7-4
http://openjpa.208410.n2.nabble.com/Which-version-of-JPA-2-0-or-2-1-does-openjpa-2-4-support-td7587858.html
maven dependencies

persistence.xml namespaces
 and default config with openJPA
 openJPA doesnt support jpa 2.1
* */